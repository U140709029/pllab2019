
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class PLProject {

	public static void main(String[] args) {
		
		LinkedList<String> linkedlist = new LinkedList<>();
		ArrayList<String> arraylist = new ArrayList<>();
		String filePath = args[0];
		
		Time("LinkedList",linkedlist,filePath);
		Time("ArrayList",arraylist,filePath);
		
	}
	public static void Time(String dataset,List<String> list, String filePath) {
		long start = 0;
		long finish = 0;

		try(Scanner scanner = new Scanner(new BufferedReader(new FileReader(filePath)))){
			
			while(scanner.hasNext()) {
				list.add(scanner.nextLine());
			}
			start = System.currentTimeMillis();
			Collections.sort(list);
			finish = System.currentTimeMillis();
		
		} catch (FileNotFoundException e) {
			System.out.println("File Not Found...");
		}
		System.out.println("////////////////////////////////////////////////////////////////\n");
		System.out.println(dataset +" Sorted Time : "+ (finish-start)+"ms");
		System.out.println("\n////////////////////////////////////////////////////////////////\n");
		/*for(String e: list) {
			System.out.println(e);
		}*/
	}

}
