from pprint import pprint
import time
import collections
import sys

print("-------------------------STACK-------------------------")
class Stack:
    def __init__(self):
        self.items = []

    def push(self, item):
        self.items.append(item)

    def pop(self):
        return self.items.pop()

    def sort(self):
        return self.items.sort()

    def print(self):
        return pprint(self.items)

dataStack = Stack()

with open(sys.argv[1],'r') as file:
    for line in file:
        line = line.rstrip('\n')
        dataStack.push(line)

#pprint(dataStack.pop())
start = time.time()
dataStack.sort()
Time = time.time() - start
print("         Stack Sorting Time: " + format(Time,'.8f'))
#dataStack.print()


################################################################

print("-------------------------QUEUE-------------------------")
dataQueue = collections.deque()

with open(sys.argv[2],'r') as file:
    for line in file:
        line = line.rstrip('\n')
        dataQueue.append(line)

#print(dataQueue.popleft())
start2 = time.time()
dataQueue = sorted(dataQueue)
Time2 = time.time() - start2
print("         Queue Sorting Time: " + format(Time2,'.8f'))
#pprint(dataQueue)
