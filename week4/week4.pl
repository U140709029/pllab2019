use strict;
use warnings;

my $x = "This is a string";
print($x,"\n");

$x = 6;				#scalar
print($x,"\n");

my @array = ("Ali",6, 7.8);     #array
print(@array,"\n");
print $array[1],"\n"; 		#same

my @array2 = ([1,2,3],[3,4,5],[7,8,9]);
print($array2[2][1],"\n");
print(@{$array2[1]},"\n");     #!!

my %student = ("Ali" => 50,"Mehmet" => 75,"Ayşe" => 80);     #hash
print($student{"Mehmet"},"\n");

my @array3 = ("one",'two','three');
my $a = @array3;
print $a,"\n";      # length of the array

my ($b,$c,$d) = @array3;
print '$b$c$d',"\n";
print "$b$c$d","\n";      # String interpolation
print $b.$c.$d,"\n";      # same result
